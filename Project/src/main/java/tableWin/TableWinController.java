package tableWin;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import sourseClass.FileWorker;
import sourses.User;

public class TableWinController {
    FileWorker fileWorker = new FileWorker("UserFile");

    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private TableView<User> usersTable;

    @FXML
    private Button loadButton;

    @FXML
    void loadFromFile(ActionEvent event) {
        ObservableList<User> userList = FXCollections.observableArrayList(fileWorker.loadUsers());
        System.out.println(userList);
        usersTable.setItems(userList);
    }

    @FXML
    void initialize() {
        TableColumn<User,String> nameColumn = new TableColumn<>("name");
        TableColumn<User,String> surnameColumn = new TableColumn<>("surname");
        TableColumn<User,String> languagesColumn = new TableColumn<>("languages");
        TableColumn<User,String> birthdayColumn = new TableColumn<>("birthday");

        nameColumn.setCellValueFactory(new PropertyValueFactory<>("name"));
        surnameColumn.setCellValueFactory(new PropertyValueFactory<>("surname"));
        languagesColumn.setCellValueFactory(new PropertyValueFactory<>("language"));
        birthdayColumn.setCellValueFactory(new PropertyValueFactory<>("birthDay"));

        nameColumn.setSortable(false);
        surnameColumn.setSortable(false);
        languagesColumn.setSortable(false);
        birthdayColumn.setSortable(false);

        usersTable.getColumns().addAll(nameColumn,surnameColumn,languagesColumn,birthdayColumn);

    }
}
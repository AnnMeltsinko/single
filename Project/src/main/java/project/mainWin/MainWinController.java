package project.mainWin;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URL;
import java.util.HashSet;
import java.util.ResourceBundle;
import java.util.Scanner;
import java.util.Set;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import sourseClass.FileWorker;
import sourses.User;

public class MainWinController {
    FileWorker fileWorker = new FileWorker("UserFile");
    Set<String> languagesSet = new HashSet<>();
    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private TextField name_field;

    @FXML
    private TextField surname_field;

    @FXML
    private ComboBox<Integer> day_field;

    @FXML
    private ComboBox<String> month_fiels;

    @FXML
    private ComboBox<Integer> year_field;

    @FXML
    private ComboBox<String> language_field;

    @FXML
    private Button complete_button;
    @FXML
    private MenuBar menu_bar;

    @FXML
    private Menu menu;

    @FXML
    private RadioMenuItem radioMenu;


    @FXML
    void completedata(ActionEvent event) throws IOException {
        String name = name_field.getText();
        String surname = surname_field.getText();
        String date = day_field.getValue() + " "+ month_fiels.getValue() +" " + year_field.getValue();
        String language = "";
        for(MenuItem menuItem : menu.getItems()){
            RadioMenuItem radioMenuItem = (RadioMenuItem)menuItem;
            if(radioMenuItem.isSelected())
                language += radioMenuItem.getText() + " ";
        }
        fileWorker.writeUser(new User(name,surname,language,date));
        name_field.setText("");
        surname_field.setText("");
    }

    @FXML
    void initialize() throws FileNotFoundException {
        birthDateComboBoxInitialization();
        menuInitialization();
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Boolean flag = false;
                    Scanner scanner = new Scanner(new File("NewLanguagesFile"));
                    while (true){
                        if(scanner.hasNextLine()){
                            String nextLan = scanner.nextLine();
                            if(languagesSet.contains(nextLan)){
                                continue;
                            }
                            addNewLanguage(nextLan);
                            languagesSet.add(nextLan);
                            continue;
                        }
                        Thread.sleep(2000);
                        scanner = new Scanner(new File("NewLanguagesFile"));

                    }
                } catch (IOException e){
                    System.out.println("Ошибка работы с файлом");
                } catch (InterruptedException ex){
                    System.out.println("поток прерван");
                }

            }
        }).start();



    }

    void birthDateComboBoxInitialization(){
        ObservableList<String> months = FXCollections.observableArrayList("December", "January", "February", "March",
                                                                                "April", "May", "June", "July", "September",
                                                                                "October", "November");
        month_fiels.getItems().addAll(months);

        ObservableList<Integer> days = FXCollections.observableArrayList();
        for (int i = 31; i >=1 ; i--)
            days.add(i);

        day_field.getItems().addAll(days);

        ObservableList<Integer> years = FXCollections.observableArrayList();
        for (int i = 2020; i >= 1900 ; i--)
            years.add(i);

        year_field.getItems().addAll(years);

    }
    void menuInitialization(){
        menu.setText("Languages");
        RadioMenuItem radioMenuItem = new RadioMenuItem("Java");
        RadioMenuItem radioMenuItem2 = new RadioMenuItem("C++");
        RadioMenuItem radioMenuItem3 = new RadioMenuItem("Python");
        RadioMenuItem radioMenuItem4 = new RadioMenuItem("C");
        RadioMenuItem radioMenuItem5 = new RadioMenuItem("JavaScript");
        languagesSet.add("Java");
        languagesSet.add("C++");
        languagesSet.add("Python");
        languagesSet.add("C");
        languagesSet.add("JavaScript");
        menu.getItems().addAll(radioMenuItem,radioMenuItem2,radioMenuItem3,radioMenuItem4,radioMenuItem5);
    }

    void addNewLanguage(String s){
        RadioMenuItem radioMenuItem = new RadioMenuItem(s);
        menu.getItems().add(radioMenuItem);
    }
}

package sourses;

import java.util.Date;

public class User {
    private String name;
    private String surname;
    private String language;
    private String birthDay;

    public User(String name, String surname, String language, String birthDay) {
        this.name = name;
        this.surname = surname;
        this.language = language;
        this.birthDay = birthDay;

    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public String getLanguage() {
        return language;
    }

    public String getBirthDay() {
        return birthDay;
    }

    @Override
    public String toString() {
        return "User{" +
                "name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", language='" + language + '\'' +
                ", birthDay='" + birthDay + '\'' +
                '}';
    }
}

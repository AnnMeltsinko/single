package sourses;

import java.util.ArrayList;
import java.util.List;

public class UsersController {
    private List<User> userList;
    public UsersController(){
        userList = new ArrayList<>();
    }

    public void addUser(User user) {
        userList.add(user);
    }

    public List<User> getUserList() {
        return userList;
    }
}

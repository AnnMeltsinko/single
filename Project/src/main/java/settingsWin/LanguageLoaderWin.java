package settingsWin;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Modality;
import javafx.stage.Stage;
import project.mainWin.WinApp;

public class LanguageLoaderWin extends Application {
    @Override
    public void start(Stage primaryStage) throws Exception {
        FXMLLoader loader = new FXMLLoader(WinApp.class.getResource("/LanguageManager.fxml"));
        Parent root = (Parent) loader.load();
        Stage stage = new Stage();
        stage.setTitle("Language loader");
        stage.initModality(Modality.APPLICATION_MODAL);
        stage.setScene(new Scene(root));
        stage.setMaxHeight(480);
        stage.setMaxWidth(688);
        stage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }
}

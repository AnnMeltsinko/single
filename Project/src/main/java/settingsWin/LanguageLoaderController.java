package settingsWin;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import sourseClass.FileWorker;

public class LanguageLoaderController {
    BufferedWriter bufferedWriter;
    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private TextField language_field;

    @FXML
    private Button add_button;

    @FXML
    void addLan(ActionEvent event) {
        if(language_field.getText().equals("")){
            language_field.setText("Введите не пустое значение");
            return;
        }
        String language = language_field.getText();
        try {
            bufferedWriter.write(language + "\n");
            bufferedWriter.flush();
        } catch (IOException e) {
            language_field.setText("Произошла ошибка записи в файл");
        }
        language_field.setText("");

    }

    @FXML
    void initialize() {
        try {
            bufferedWriter = new BufferedWriter(new FileWriter(new File("NewLanguagesFile"),true));
        } catch (IOException e) {
            language_field.setText("Файл для записи некорректен");
        }

    }
}

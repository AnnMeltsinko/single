package sourseClass;

import com.google.gson.Gson;
import sourses.User;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class FileWorker {
    private File file;
    private BufferedWriter bufferedOutputStream;
    private Scanner scanner;

    public FileWorker(String filePath){
        file = new File(filePath);
        try {
            bufferedOutputStream = new BufferedWriter(new FileWriter(file,true));
        } catch (IOException e) {
            System.out.println("Файл по заданному пути не найден!");
        }
    }
    public void writeUser(User user){
        try {
            bufferedOutputStream.write(new Gson().toJson(user) + "\n");
            bufferedOutputStream.flush();
        } catch (IOException e) {
            System.out.println("Не удалось записать в файл!");
        }
    }
    public List<User> loadUsers(){
        try {
            scanner = new Scanner(file);
        } catch (FileNotFoundException e) {
            System.out.println("Ошибка с файлом");
        }
        List<User> userList = new ArrayList<>();
        while(scanner.hasNext()){
            userList.add(new Gson().fromJson(scanner.nextLine(),User.class));
        }
        return userList;
    }
}

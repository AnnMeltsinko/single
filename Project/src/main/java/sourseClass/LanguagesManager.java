package sourseClass;

import java.util.ArrayDeque;
import java.util.PriorityQueue;
import java.util.Queue;

public class LanguagesManager {
    Queue<String> lanQueue = new PriorityQueue<>();
    public void addNewLanguage(String s){
        lanQueue.add(s);
    }
    public String getLanguageFromQueue(){
        return lanQueue.poll();
    }
}
